// console.log("Hello World");

// [SECTION] Objects

// 
// Syntax: let objectName = {
// 	keyA: valueA,
// 	keyA: valueA
// }

let cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
}
console.log("Result from creating objects using initializers/literal notation");  //initializers and literal notation {}
console.log(cellphone);
console.log(typeof cellphone);

// "this" inside an object

function Laptop(name, manufacturedDate){
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}

let laptop = new Laptop ("Lenovo", 2008);
console.log("Result from creating objects using obejet constructors");
// obejet constructors --> "this" and "new"
console.log(laptop);

// Re-using Laptop function
let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using obejet constructors");
console.log(myLaptop);

let oldlaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating objects using obejet constructors");
console.log(oldlaptop);  //this will result to undefined 

// Creating empty object
let computer = {}
let myComputer = new Object();

// [SECTION] Accessing Obeject Properties
//DOT NOTATION  --> to access particular property in an object

console.log("Result from dot notation: " + myLaptop.name);
console.log("Result from dot notation: " + myLaptop.manufacturedDate);

// Square bracket notation
console.log("Result from square bracket notation: " + myLaptop['name']);

// Accessing array objects

let array = [laptop, myLaptop];

// Accessing array using indexes
console.log(array[0]['name']);
console.log(array[0].name);

// [SECTION] Initializing, Adding, Deleting, Re-assigning Object Properties

let car = {};
console.log(car);   //Hoisting --> Reading a property ahead of its initialization

// Initializing or Adding property --> using dot notation

car.name  = "Honda Civic";
console.log("Result from adding a property using dot notation:");
console.log(car);

// car.manufacturedDate = 2019;
// console.log("Result from adding a property using dot notation:");
// console.log(car);

// Initializing or Adding property using bracket notation

car['manufacturedDate']=2019;
console.log(car.manufacturedDate);
console.log("Result from adding a property using square bracket notation: ");
console.log(car);


// Delete object property
delete car['manufacturedDate'];
console.log("Result from deleting properties:")
console.log(car);

// for dot

delete car.manufacturedDate;
console.log("Result from deleting properties:")
console.log(car);

// Re-assigning object properties --> modifying /updating
car.name = "Dodge Charger R/T";
console.log("Result from re-assigning")
console.log(car);

// [SECTION] Object Method


let person = {
	name: "April",
	talk: function(){
		console.log("Hello my name is " + this.name);
		}
}
console.log(person);
console.log("Result from object method");
person.talk();    //Invocation/calling a function

// Add a property with an object method/function
person.walk = function(){
	console.log(this.name + " walked 25 steps forward " )
}
person.walk();

// Another example
//Nested properties inside an object

let friend = {
	firstName: "April",
	lastName: "Ng",
	address: {
		city: "New York",
		country: "USA",
	},
	emails: ["amgersaniva@gmail.com", "aprilmariegersaniva@gmail.com"],
	introduce: function(){
		console.log("Hello my name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();


// [SECTION] Real World Application of Objects

// Scnario



let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled taegetPokemn");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	},
	faint: function(){
		console.log("pokemon fainted");
	}
}

console.log(myPokemon);

// 
function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level; //32
	this.attack = level; //16

	//Methods/Function
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now to _targetPokemonHealth_");
		this.faint = function(){
			console.log(this.name + "fainted");
	}
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
